package com.fairbilling.mavenprj;

public class UserSessionData {
	
	private String userId;
	private int numberOfSessions;
	private int billingTimeInSeconds;
	
	public UserSessionData(String userId, int numberOfSessions, int billingTimeInSeconds) {
		this.userId = userId;
		this.numberOfSessions = numberOfSessions;
		this.billingTimeInSeconds = billingTimeInSeconds;		
	}
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}
	/**
	 * @return the numberOfSessions
	 */
	public int getNumberOfSessions() {
		return numberOfSessions;
	}
	/**
	 * @param numberOfSessions the numberOfSessions to set
	 */
	public void setNumberOfSessions(int numberOfSessions) {
		this.numberOfSessions = numberOfSessions;
	}
	/**
	 * @return the billingTimeInSeconds
	 */
	public int getBillingTimeInSeconds() {
		return billingTimeInSeconds;
	}
	/**
	 * @param billingTimeInSeconds the billingTimeInSeconds to set
	 */
	public void setBillingTimeInSeconds(int billingTimeInSeconds) {
		this.billingTimeInSeconds = billingTimeInSeconds;
	}

}
