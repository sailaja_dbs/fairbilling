package com.fairbilling.mavenprj;

import java.time.LocalTime;

public class UserSession {
	
	private String userId;
	private LocalTime startTime;
	private LocalTime endTime;
	
	public UserSession() {}
	
	public UserSession(String userId) {
		this.userId = userId;
	}
	
	public UserSession(String userId, LocalTime startTime, LocalTime endTime) {
		this.userId = userId;
		this.startTime = startTime;
		this.endTime = endTime;
	}
	
	
	/**
	 * @return the userId
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * @return the startTime
	 */
	public LocalTime getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime the startTime to set
	 */
	public void setStartTime(LocalTime startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */
	public LocalTime getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime the endTime to set
	 */
	public void setEndTime(LocalTime endTime) {
		this.endTime = endTime;
	}

	// Generated toString
	@Override
	public String toString() {
		return "UserSession [userId=" + userId + ", startTime=" + startTime + ", endTime=" + endTime + "]";
	}
	
	
}