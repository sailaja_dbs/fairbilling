package com.fairbilling.mavenprj;

import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class BillingComputation {
	
	public static final String START_ACTION = "Start";
	public static final String END_ACTION = "End";
	
	public BillingComputation() {}

	public static List<UserSessionData> processAsList(List<String> lines) {
		// TODO Auto-generated method stub

		if (lines == null || lines.isEmpty()) {
			System.err.println("Your File is empty ");
			return new ArrayList<>();
		}

		List<LineInPieces> piecesList = breakUpAllTheLines(lines);
		//System.out.println("piecesList == Step 3::: "+piecesList);

		// Determine the  first and last time. 
		LocalTime firstTimeInFile = null;
		LocalTime lastTimeInFile = null;
		if (piecesList.size() > 0) {
			firstTimeInFile = 
					LocalTime.parse(piecesList.get(0).getHours() + ":" 
							+ piecesList.get(0).getMinutes() + ":" 
							+ piecesList.get(0).getSeconds());
			lastTimeInFile = 
					LocalTime.parse(piecesList.get(piecesList.size()-1).getHours() + ":" 
							+ piecesList.get(piecesList.size()-1).getMinutes() + ":" 
							+ piecesList.get(piecesList.size()-1).getSeconds());

		}

		// Process list into map of UserSession records
		Map<String, List<UserSession>> map = processLines(piecesList);

		List<UserSessionData> results = new ArrayList<>();

		// Calculate how long each session lasted in seconds
		for (String userid : map.keySet()) {
			int total = 0;
			int numberOfSessions = 0;
			for (UserSession us : map.get(userid)) {
				numberOfSessions++;
				if (us.getStartTime() == null) { 
					us.setStartTime(firstTimeInFile);
				}
				if (us.getEndTime() == null) { 
					us.setEndTime(lastTimeInFile);
				}

				total += + Duration.between(us.getStartTime(),us.getEndTime()).getSeconds();
			}

			results.add( new UserSessionData(userid, numberOfSessions, total));
		}
		return results;
	}
	
	

	public static Map<String, List<UserSession>> processLines(List<LineInPieces> lines) {

		Map<String, List<UserSession>> userSessionMap = new LinkedHashMap<>();
		for (LineInPieces line : lines) {			
			// Get the sessions for the user
			List<UserSession> userSessionList = userSessionMap.get(line.getUserid());				
			// Process into the sessions array
			userSessionList = processEachLine(line, userSessionList);
			userSessionMap.put(line.getUserid(), userSessionList);
		}

		return userSessionMap;
	}

	private static List<UserSession> processEachLine(LineInPieces line, List<UserSession> userSessionList) {
		// TODO Auto-generated method stub
		if (userSessionList == null) {
			userSessionList = new ArrayList<UserSession>();
		}
		LocalTime lineTime = LocalTime.parse(line.getHours() + ":" + line.getMinutes() + ":" + line.getSeconds());
		//Check for start and add 
		if (START_ACTION.equals(line.getAction()) ) {
			UserSession userSession = new UserSession(line.getUserid());
			userSession.setStartTime(lineTime);
			userSessionList.add(userSession);
			return userSessionList;
		}
		//if not start ,for end calculate data ,
		for (UserSession userSession: userSessionList) {
			
			// Matches first Start - is it unfinished?
			if (userSession.getEndTime() == null) {
				userSession.setEndTime(lineTime);
				return userSessionList;
			}
		}

		// Otherwise just add a new End record 
		UserSession userSession = new UserSession(line.getUserid());
		userSession.setEndTime(lineTime);
		userSessionList.add(userSession);
		return userSessionList;
	}

	/** 
	 *  Process the list of lines 
	 */	
	protected static List<LineInPieces> breakUpAllTheLines(List<String> lines) {

		List<LineInPieces> lineInPiecesList = new ArrayList<>();

		// Process all lines
		for (String singleRecord : lines) {
			LineInPieces lineInPieces = new LineInPieces();
			lineInPieces = getEachLineData(singleRecord);			

			// Validate action
			if (!START_ACTION.equals(lineInPieces.getAction()) && !END_ACTION.equals(lineInPieces.getAction())) {
				System.err.println("Invalid action: action not set to either Start or End in line " + singleRecord + " - skipped");
				lineInPieces.setValid(false);
			}

			// Ignore invalid lines
			if (lineInPieces.isValid()) {
				lineInPiecesList.add(lineInPieces);
			}
		}

		return lineInPiecesList;
	}
	
	/**
	 * Break up the line.  If it's valid, it will be like "HH:MM:SS ##### ####"
	 */
	protected static LineInPieces getEachLineData(String line) {

		LineInPieces lineInPieces = new LineInPieces();
		lineInPieces.setValid(false);

		if (line == null || line.isEmpty()) {
			return lineInPieces;
		}

		String patternString = "^(\\d\\d):(\\d\\d):(\\d\\d) (.*) (.*)$";
		Pattern pattern = Pattern.compile(patternString);		
		Matcher matcher = pattern.matcher(line);

		while(matcher.find()) {
			lineInPieces.setHours( matcher.group(1) );
			lineInPieces.setMinutes( matcher.group(2) );
			lineInPieces.setSeconds( matcher.group(3) );
			lineInPieces.setUserid( matcher.group(4) );
			lineInPieces.setAction( matcher.group(5) );
		}

		lineInPieces.setValid(matcher.matches());
		return lineInPieces;
	}
	


}
