package com.fairbilling.mavenprj;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;





public class FairBilling
{
    public static void main( String[] args )
    
    {
    	// Arguments  passed while running
    	//if (args.length < 1) {
    		//System.err.println("Wrong number of arguments " + args.length + ": syntax is \nSampleFairBilling <path to file>");
    	//}
    	//String fileName = args[args.length - 1];
		String fileName = "../SampleFairBilling/test.log";
    	
    	    	
    	FairBilling fairBilling = new FairBilling();
    	
    	try {
    		// Load the file into a list
    		List<String> lines = fairBilling.loadFileToList(fileName);
    
    		
    		// Process list
	    	List<UserSessionData> results = BillingComputation.processAsList(lines);
	    	
	    	// Results
	    	for (UserSessionData UserSessionData : results) {
	    		
	    		System.out.println(" UserName: " +UserSessionData.getUserId() + " No.Sessions: " + UserSessionData.getNumberOfSessions() + " Billing Time in Seconds: " + UserSessionData.getBillingTimeInSeconds());
	    	}
	    	
    	} catch (Exception e) {
    		// TODO Auto-generated catch block
    		e.printStackTrace();
    	}
    	
    }

	

	List<String> loadFileToList(String fileName) throws Exception {

		List<String> lines = Collections.emptyList(); 
		try {
			lines = Files.readAllLines(Paths.get(fileName), StandardCharsets.UTF_8);
		} catch (NoSuchFileException n) {
			throw new Exception("No such file " + fileName, n);
		} catch (IOException e) {
			throw new Exception("File error " + fileName, e);
		} 
		return lines; 
	}



}
