package com.fairbilling.mavenprj;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;


/**
 * Unit test for simple App.
 */
public class FairBillingTest 
{
	private FairBilling fairBilling;
	private BillingComputation billingComputation;
    
	@Before
	public void setup() {
		fairBilling = new FairBilling();
	}
	
	
     ///********** Check the file 
	@Test
    public void testLoadFile() throws Exception {	
		List<String> file = fairBilling.loadFileToList("test.log");		
        assertFalse( file.isEmpty() );
        assertEquals("Test file loaded correctly", 11, file.size());
    }
	
	@Test
    public void testLoadNoFile() {	
		try {
			fairBilling.loadFileToList("XXXXX");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
    }
	
	 ///**********  get each line data and validate
	
		@Test
	    public void testNullOrEmptyLine() {
			assertFalse(billingComputation.getEachLineData(null).isValid());		
			assertFalse(billingComputation.getEachLineData("").isValid());		
	    }
		
		@Test
	    public void testInvalidLine() {
			assertFalse(billingComputation.getEachLineData("xxx").isValid());		
	    }
		
		@Test
	    public void testInvalidLinePartial() {
			assertFalse(billingComputation.getEachLineData("14:02:03 ALICE99").isValid());		
	    }
		
		@Test
	    public void testValidLine() {
			assertTrue(billingComputation.getEachLineData("14:02:03 ALICE99 Start").isValid());		
	    }
		
		//********** Check each line data with one end , one start and different cases statuses
		
		
		/*@Test
		public void testProcessFileOneEndOneRow() {
			List<LineInPieces> pList = Arrays.asList(
			       new LineInPieces("14","02","11","ALICE99","End"));
			
			Map<String, List<UserSession>> map = billingComputation.processLines(pList);
			assertNotNull(map);
			assertEquals(1, map.keySet().size());
			assertEquals(1, map.get("ALICE99").size());
			assertEquals("[UserSession [userId=ALICE99, startTime=null, endTime=14:02:11]]", map.get("ALLICE99").toString());
		}*/
		
		@Test
		public void testProcessWithTwoEndsTwoRows() {
			List<LineInPieces> pList = Arrays.asList(
			       new LineInPieces("14","02","11","ALICE99","End"),
			       new LineInPieces("14","02","12","ALICE99","End"));
			
			Map<String, List<UserSession>> map = billingComputation.processLines(pList);
			assertEquals(2, map.get("ALICE99").size());
		}
		
		@Test
		public void testProcessWithTwoStartsTwoRows() {
			List<LineInPieces> pList = Arrays.asList(
			       new LineInPieces("14","02","11","ALICE99","Start"),
			       new LineInPieces("14","02","12","ALICE99","Start"));
			
			Map<String, List<UserSession>> map = billingComputation.processLines(pList);
			assertEquals(2, map.get("ALICE99").size());
		}
		
		
		@Test
		public void testProcessWithNullFileAsList() {
			List<UserSessionData> results = billingComputation.processAsList(null);
			assertEquals(0, results.size());
		}

		@Test
		public void testProcessWithEmptyFileAsList() {
			List<UserSessionData> results = billingComputation.processAsList(new ArrayList<>());
			assertEquals(0, results.size());
		}
		
		@Test
		public void testWithOneRow() {
			List<UserSessionData> results = billingComputation.processAsList(Arrays.asList("14:00:00 ALICE99 Start"));
			assertEquals(1, results.size());
			assertEquals("UserResult [userId=ALICE99, numberOfSessions=1, billingTimeInSeconds=0]", results.get(0).toString());
		}
		
		@Test
		public void testWithTwoRows() {
			List<UserSessionData> results = billingComputation.processAsList(
					Arrays.asList("14:00:00 ALICE99 Start",
							      "14:00:01 ALICE99 End"));
			assertEquals(1, results.size());
			assertEquals("UserResult [userId=ALICE99, numberOfSessions=1, billingTimeInSeconds=1]", results.get(0).toString());
		}
		
		@Test
		public void testWithThreeRows() {
			List<UserSessionData> results = billingComputation.processAsList(
					Arrays.asList("14:00:00 ALICE99 Start",
							      "14:00:01 ALICE99 End",
							      "14:00:02 ALICE99 End"));
			assertEquals(1, results.size());
			assertEquals("UserResult [userId=ALICE99, numberOfSessions=2, billingTimeInSeconds=3]", results.get(0).toString());
		}
		
	
}
